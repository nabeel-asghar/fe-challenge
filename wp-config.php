<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_challenge');

/** MySQL database username */
define('DB_USER', 'fe_challenge');

/** MySQL database password */
define('DB_PASSWORD', 'fe_challenge');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}[2_LBE#k(pP2:H9`j[9%`zTB0u.}V;;z0Sv79HMzWvZyz}5uV%Odr@cEj&bFS{g');
define('SECURE_AUTH_KEY',  'F:pnPTgK@OcDf%UvD>EIgdeH<}&&|Eo[4%N!jf{5`9EhJEU*}(.%A0pU15903D=v');
define('LOGGED_IN_KEY',    'ot;Y&HBAx**-ymMgV2rwewOfw^(w4KaqV]%^q=Go8[TkQPW$DKTfnDT4THqRB[qD');
define('NONCE_KEY',        'zLwizd7MyIk$d/+17=zv*jMQ]K2U b+^G;3zM,|j#!)*kFXBDSpzz~jR7U7aPbo;');
define('AUTH_SALT',        'EN6/g6s]Q.ygz#$(J&ERUNg7M*NVtYN87 8,M#+v HT0I:{ dIE][$FU6WFCu=EL');
define('SECURE_AUTH_SALT', 'Ddd[&2Z(a&zO4wlFPuk.1PB8H5D/3? CT8S8,H#M{%Ie%roaHM+!/e$Alm[O_@3S');
define('LOGGED_IN_SALT',   ',f/tTV3K*9-QxoM@v$$bB8z{^?ZoiN5!V<pqL3:o gGE&%CbMU=(VBGCS5Dk|390');
define('NONCE_SALT',       'fiAi.IHL5sf]gQ/jf@/>!Ks$_K6wb_CR!W!:P_jg~y^F!sDAndyVe}so7g4b17Xa');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
